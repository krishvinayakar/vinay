import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import ButtonBase from "@material-ui/core/ButtonBase";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Button from "@material-ui/core/Button";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import AddIcon from "@material-ui/icons/Add";
import HomeIcon from "@material-ui/icons/Home";
import Box from "@material-ui/core/Box";
import LocalOfferIcon from "@material-ui/icons/LocalOffer";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import TextField from "@material-ui/core/TextField";
import { withStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import InputBase from "@material-ui/core/InputBase";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import Rating from "@material-ui/lab/Rating";
import InfoSharpIcon from "@material-ui/icons/InfoSharp";
import Divider from "@material-ui/core/Divider";
import Container from "@material-ui/core/Container";
import Link from "@material-ui/core/Link";
import ButtonGroup from '@material-ui/core/ButtonGroup';

const brand = [
    {
        value: "1",
        label: "Regular Delivery",
    },
    {
        value: "2",
        label: "Free Delivery",
    },
];
function getSteps() {
    return ["Cart", "Address", "Payment"];
}


export default function Cart() {
    const [activeStep, setActiveStep] = React.useState(0);
    const steps = getSteps();
    const [currency, setCurrency] = React.useState("");

    const handleChange = (event) => {
        setCurrency(event.target.value);
    };
    const handleChanges = (event) => {
        setProducts(event.target.value);
    };

    return (
        <div className="cart_header">
            <Grid container>
                <Grid item xs={12} sm={12}>
                    <Stepper activeStep={activeStep} alternativeLabel>
                        {steps.map((label) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                </Grid>
            </Grid>

            <Grid container spacing={1} className="cart_heads">
                <Grid item xs={12} sm={7} md={7} lg={7} className="cart_wishlist">
                    <Box p={1} className="cart_banner">
                        <Box p={1} className="cart_flexing">
                            <ShoppingCartIcon fontSize="small" />
                            My Cart (2)
                        </Box>
                    </Box>
                    <Grid container spacing={1} className="cart_details">
                        <Grid item xs={12} lg={2}>
                            <img alt="complex" src="asset/mycart_cart1.png" />
                        </Grid>
                        <Grid item xs={12} sm={12} md={12} lg={10}>
                            <Grid container spacing={1} className="cart_divided">
                                <Grid item xs={12} sm={8} md={8} lg={8} className="cart_product">
                                    <Typography variant="subtitle1">one plus 8 (glacier green, 128gb)</Typography>
                                    <Typography className="cart_ram" variant="body1" gutterBottom>
                                        8gb ram
                                    </Typography>
                                    <Typography variant="body2" className="cart_rate" color="textSecondary">
                                        <span>
                                            <FontAwesomeIcon icon="rupee-sign" />
                                            1,19,249
                                        </span>
                                        <span>
                                            <FontAwesomeIcon icon="rupee-sign" fontSize="small" />
                                            <del>1,19,249</del> (4%OFF)
                                        </span>
                                        <span>
                                            9 offers available <InfoSharpIcon fontSize="small" />
                                        </span>
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} sm={4} md={4}lg={4}>
                                    <TextField
                                        id="outlined-select-currency-native"
                                        select
                                        value={currency}
                                        onChange={handleChange}
                                        className="cart_delivery"
                                        SelectProps={{
                                            native: true,
                                        }}
                                    >
                                        {brand.map((option) => (
                                            <option key={option.value} value={option.value}>
                                                {option.label}
                                            </option>
                                        ))}
                                    </TextField>
                                    <Typography variant="body1" className="cart_free">
                                        Free<span>Delivery by Nov 20</span>
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Box className="cart_summary" p={1}>
                                <Box p={1} className="cart_flexing">
                                    <ButtonGroup  aria-label="outlined primary button group">
  <Button>-</Button>
  <Button>0</Button>
  <Button>+</Button>
</ButtonGroup>
                                </Box>
                                <Box p={1} className="cart_item">
                                    Move to Wishlist
                                </Box>
                                <Box p={1}>
                                    <Typography variant="subtitle1" className="cart_moves">
                                        Remove <DeleteIcon size="small" />
                                    </Typography>
                                </Box>
                            </Box>
                        </Grid>
                        <div className="cart_protitles">
                        <div className="cart_prowrp">
                            <div className="cart_protectioncontent">
                                <div className="cart_protfirst">
                                    <img src="asset/mycart_protection.png"></img>
                                </div>
                                <div className="cart_protsecond">
                                    <Typography variant="subtitle1" gutterBottom>
                                        complete mobile protection
                                    </Typography>

                                    <Typography variant="body2" className="cart_rates" color="textSecondary">
                                        <span>
                                            <FontAwesomeIcon icon="rupee-sign" />
                                            5,000
                                        </span>
                                        <span>
                                            <FontAwesomeIcon icon="rupee-sign" fontSize="small" />
                                            <del>1,33,576</del>{" "}
                                        </span>
                                        <span>(4%OFF)</span>
                                    </Typography>
                                    <Typography variant="body2" className="cart_rates">
                                        Brand authorised repair / replacement guarantee for 1 year.Special offer "Get 6 month Google One trial with complete mobile protection"
                                        <span>Know More</span>
                                    </Typography>
                                    <Button variant="contained" className="cart_protect">
                                        Add Protection
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </div>


                    </Grid>
                    <Grid container spacing={1} className="cart_details">
                        <Grid item xs={12} lg={2}>
                            <img alt="complex" src="asset/mycart_cart2.png" />
                        </Grid>
                        <Grid item xs={12} sm={12} md={12} lg={10}>
                            <Grid container spacing={1} className="cart_divided">
                                <Grid item xs={12} sm={8} md={8} lg={8} className="cart_product">
                                    <Typography variant="subtitle1">one plus wireless z earphones</Typography>
                                    <Typography className="cart_ram" variant="body1" gutterBottom>
                                        black
                                    </Typography>
                                    <Typography variant="body2" className="cart_rate" color="textSecondary">
                                        <span>
                                            <FontAwesomeIcon icon="rupee-sign" />
                                           3,234
                                        </span>
                                        <span>
                                            
                                        </span>
                                        <span>
                                            9 offers available <InfoSharpIcon fontSize="small" />
                                        </span>
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} sm={4} md={4} lg={4}>
                                    <TextField
                                        id="outlined-select-currency-native"
                                        select
                                        value={currency}
                                        onChange={handleChange}
                                        className="cart_delivery"
                                        SelectProps={{
                                            native: true,
                                        }}
                                    >
                                        {brand.map((option) => (
                                            <option key={option.value} value={option.value}>
                                                {option.label}
                                            </option>
                                        ))}
                                    </TextField>
                                    <Typography variant="body1" className="cart_free">
                                        Free<span>Delivery by Nov 20</span>
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Box className="cart_summary" p={1}>
                                <Box p={1} className="cart_flexing">
                                   
                                    <ButtonGroup  aria-label="outlined primary button group">
  <Button>-</Button>
  <Button>0</Button>
  <Button>+</Button>
</ButtonGroup>
                                </Box>
                                <Box p={1} className="cart_item">
                                    Move to Wishlist
                                </Box>
                                <Box p={1}>
                                    <Typography variant="subtitle1" className="cart_moves">
                                        Remove <DeleteIcon size="small" />
                                    </Typography>
                                </Box>
                            </Box>
                        </Grid>
                        

                        
                        

                    </Grid>
                    
                </Grid>

                <Grid item xs={12} sm={5} md={5} lg={5} className="cart_summarys">
                    <div className="cart_payments">
                        <Typography variant="h6" gutterBottom>
                            Payment Summary
                        </Typography>

                        <div className="cart_amount">
                            <Box className="cart_sum" p={1}>
                                <Box p={1} className="cart_flexing">
                                    Price - 2 Items
                                </Box>
                                <Box p={1}>1,06,600</Box>
                            </Box>
                            <Box className="cart_sum" p={1}>
                                <Box p={1} className="cart_flexing">
                                    Delivery Fees
                                </Box>
                                <Box p={1} className="cart_free">
                                    Free
                                </Box>
                            </Box>
                            <Box className="cart_sum" p={1}>
                                <Box p={1} className="cart_flexing">
                                    Coupon Discount
                                </Box>
                                <Box p={1} className="cart_coupon">
                                    Apply Coupon
                                </Box>
                            </Box>

                            <div className="cart_total">
                                <Box className="cart_sum" p={1}>
                                    <Box p={1} className="cart_flexing">
                                        Total Amount
                                    </Box>
                                    <Box p={1}>1,06,600</Box>
                                </Box>
                                <Typography variant="subtitle2" gutterBottom>
                                    <LocalOfferIcon fontSize="small" />
                                    You will be save 5,000 in this order
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className="cart_payments ">
                        <Typography variant="h6" className="cart_gift" gutterBottom>
                            <FontAwesomeIcon icon="gift" />
                            Use Gift Certificate
                        </Typography>
                        <Typography variant="h6" className="cart_coupon" gutterBottom>
                            <TextField className="cart_couponcode"  placeholder="Enter Your Gift Certificate Code" inputProps={{ 'aria-label': 'description' }}/>
                            <Button variant="contained" className="cart_button">
                                Apply Gift Certificate
                            </Button>
                        </Typography>
                        <div></div>
                    </div>

                    <Button variant="contained" className="cart_checkout">
                        Proceed To Checkout
                    </Button>
                </Grid>
            </Grid>

            <Grid container spacing={2} className="cart_heads">
                <Grid item xs={12} sm={7} md={7} lg={7} className="cart_wishlist">
                    <Box p={1} className="cart_banner">
                        <Box p={1} className="cart_flexing">
                            <FavoriteBorderIcon />
                            My Wishlist
                        </Box>
                    </Box>
                    
                    <Grid item xs={12} sm={12} md={12} lg={12}  container className="card_paper">
                        <Grid item>
                            <img alt="complex" src="asset/mycart_cart4.png" />
                        </Grid>

                        <Grid item xs container direction="column" className="cart_datas">
                            <Grid item xs>
                                <Typography gutterBottom variant="subtitle1">
                                    Apple Watch Series 3 GPS - 38mm Space Grey Aluminium Case with Black Sport Band
                                </Typography>
                                <Typography variant="body2" gutterBottom>
                                    4.6 <Rating name="read-only" size="small" value={4} className="cart_rating" readOnly />
                                </Typography>

                                <Typography variant="body2" className="cart_price" color="textSecondary">
                                    <FontAwesomeIcon icon="rupee-sign" />
                                    1,19,249
                                </Typography>
                            </Grid>
                        </Grid>

                        <Grid item>
                            <Typography variant="subtitle1" className="cart_move">
                                Move To Cart
                            </Typography>
                            <Typography variant="subtitle1" className="cart_moves">
                                Remove <DeleteIcon size="small" />
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12} container className="card_paper">
                        <Grid item>
                            <img alt="complex" src="asset/mycart_cart3.png" />
                        </Grid>

                        <Grid item xs container direction="column" className="cart_billing">
                            <Grid item xs>
                                <Typography gutterBottom variant="subtitle1">
                                    Lenovo Ideapad S145 Intel Core I3 7th Gen Windows 10 Laptop (4GB RAM, 1TB HDD, 15.6 inch, Platinum Grey, 1.85kg)
                                </Typography>
                                <Typography variant="body2" gutterBottom>
                                    4.6 <Rating name="read-only" size="small" value={4} className="cart_rating" readOnly />
                                </Typography>

                                <Typography variant="body2" className="cart_price" color="textSecondary">
                                    <FontAwesomeIcon icon="rupee-sign" />
                                    31,000
                                </Typography>
                            </Grid>
                        </Grid>

                        <Grid item>
                            <Typography variant="subtitle1" className="cart_move">
                                Move To Cart
                            </Typography>
                            <Typography variant="subtitle1" className="cart_moves">
                                Remove <DeleteIcon size="small" />
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}
