import React from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { resetIdCounter } from 'react-tabs';
import Grid from '@material-ui/core/Grid';
import Typography from "@material-ui/core/Typography";
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
resetIdCounter();
import './product.css';
import img1 from './images/product1.png';
import img2 from './images/product2.png';
import img3 from './images/product3.png';
import img4 from './images/product4.png';
import { withCssResources } from '@storybook/addon-cssresources';
const images1 = {
src:img1,
alt:'product1',
};
const images2 = {
src:img2,
alt:'product2',
};
const images3 = {
src:img3,
alt:'product3',
};
const images4 = {
src:img4,
alt:'product4',
};





export default 
{
	title:'Example/product',
	component:products,
};

export const products = () =>
{
	
    return (
       
                           <div className="Boughttogether-main-wrp">
              
              <Tabs defaultIndex={0} className="bought-tab">

               

                <TabList>

                    <div className="brought-title">
                    <h4>Want to Protect your Phone ?</h4>
                    </div>

                <div className="brought-item">
                <Tab>All</Tab>
                <Tab>Cases & Covers</Tab>
                <Tab>Airpods</Tab>
                <Tab>Apple Watch</Tab>
                <Tab>Pouches</Tab>
                </div>

                </TabList>
            
                <div className="brought-content">
                <TabPanel>
                <Grid container>   
                <Grid item xs={12} sm={3} lg={3}>
                    <div className="inner-brought-item">

                        <div className="icon-offer">
                            <FavoriteBorderIcon />
                            <Typography variant="subtitle2" className="share" gutterBottom>4% OFF </Typography>
                        </div>

                        <img src={images1.src} alt={images1.alt} />
                        <div className="rat-bought">
                        <Typography variant="body1">Amazfit Bip S Smartwatch (Black)</Typography>
                        <h4>16,999 <span><strike>25,499</strike></span> </h4>  
                        <h5>Save Rs 3,427</h5> 
                        </div>
                    </div>
                </Grid>
                <Grid item xs={12} sm={3} lg={3}>
                    <div className="inner-brought-item">

                        <div className="icon-offer">
                            <FavoriteBorderIcon />
                            <Typography variant="subtitle2" className="share" gutterBottom>4% OFF </Typography>
                        </div>

                        <img src={images2.src} alt={images2.alt} />
                        <div className="rat-bought">
                        <Typography variant="body1">MI Smart Tv 32inch (Black) </Typography>
                        <h4>26,599 <span><strike>59,999</strike></span> </h4>  
                        <h5>Save Rs 3,427</h5> 
                        </div>
                    </div>
                </Grid>
                <Grid item xs={12} sm={3} lg={3}>
                    <div className="inner-brought-item">

                        <div className="icon-offer">
                            
                            <Typography variant="subtitle2" className="share" gutterBottom>4% OFF </Typography>
                        </div>

                        <img src={images3.src} alt={images3.alt} />

                        <div className="rat-bought">
                        <Typography variant="body1">Apple iphone 11Pro </Typography>
                        <h4>30,999 <span><strike>35,999</strike></span> </h4>  
                        <h5>Save Rs 3,427</h5> 
                        </div>
                    </div>
                </Grid>
                <Grid item xs={12} sm={3} lg={3}>
                    <div className="inner-brought-item">

                        <div className="icon-offer">
                            <FavoriteBorderIcon />
                            <Typography variant="subtitle2" className="share" gutterBottom>4% OFF </Typography>
                        </div>

                        <img src={images4.src} alt={images4.alt} />
                        <div className="rat-bought">
                        <Typography variant="body1">Apple Watch Series 6 </Typography>
                        <h4>6,999 <span><strike>9,999</strike></span> </h4>  
                        <h5>Save Rs 3,427</h5> 
                        </div>
                    </div>
                </Grid>
                </Grid>
                </TabPanel>
                <TabPanel>
                <Grid item xs={12} sm={3} lg={3}>
                    <div className="inner-brought-item">
                        <img src={images1.src} alt={images1.alt} />
                        <div className="rat-bought">
                        <Typography variant="body1">Apple Watch Series 6 </Typography>
                        <h4>6,999 <span><strike>9,999</strike></span> </h4>  
                        <h5>Save Rs 3,427</h5> 
                        </div>
                    </div>
                </Grid>
                </TabPanel>
                <TabPanel>
                <Grid container>  
                <Grid item xs={12} sm={3}>
                    <div className="inner-brought-item">
                       <img src={images1.src} alt={images1.alt} />
                        <div className="rat-bought">
                        <Typography variant="body1">Apple Watch Series 6 </Typography>
                        <h4>6,999 <span><strike>9,999</strike></span> </h4>  
                        <h5>Save Rs 3,427</h5> 
                        </div>
                    </div>
                </Grid>
                </Grid>
                </TabPanel>
                <TabPanel>
                <Grid container>     
                <Grid item xs={12} sm={3}>
                    <div className="inner-brought-item">
                        <img src={images1.src} alt={images1.alt} />
                        <div className="rat-bought">
                        <Typography variant="body1">Apple Watch Series 6 </Typography>
                        <h4>6,999 <span><strike>9,999</strike></span> </h4>  
                        <h5>Save Rs 3,427</h5> 
                        </div>
                    </div>
                </Grid>
                </Grid>
                </TabPanel>
                <TabPanel>
                <Grid item xs={12} sm={3}>
                <Grid container>
                    <div className="inner-brought-item">
                       <img src={images1.src} alt={images1.alt} />
                        <div className="rat-bought">
                        <Typography variant="body1">Apple Watch Series 6 </Typography>
                        <h4>6,999 <span><strike>9,999</strike></span> </h4>  
                        <h5>Save Rs 3,427</h5> 
                        </div>
                    </div>
                </Grid>
                </Grid>
                </TabPanel>
                </div>
            </Tabs>
            
        </div>
    );
}