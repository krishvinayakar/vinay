import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import AddIcon from "@material-ui/icons/Add";
import TextField from "@material-ui/core/TextField";
import Divider from '@material-ui/core/Divider';
import Link from '@material-ui/core/Link';
import Box from "@material-ui/core/Box";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import './protection.css';
import img1 from './images/mycart_protection.png';
import { withCssResources } from '@storybook/addon-cssresources';
const images1 = {
src:img1,
alt:'protection1',
};




export default 
{
	title:'Example/protection',
	component:protections,
    decorators: [withCssResources],
};

export const protections = () =>
{
	
    return (
       
                           <div className="pro-title">
                              <Typography variant="h4" gutterBottom> Want to Protect Your Phone ? </Typography>
                           
                           <div className="protection-main">
                            <div className="inner-wrp">
                           <div className="pro-wrp">
                           <Checkbox className="checkwrp"/>
                           <div className="protection-content">
                            <div className="prot-first">
                               
                                <img src={images1.src} alt={images1.alt}></img>
                            </div>
                           <div className="prot-second">
                                <Typography variant="h6" gutterBottom> 1 Year Accidental, Liquid and Screen Production Plan</Typography>
                                <h5>3,847 <span><strike>6,800</strike></span> </h5>
                           </div>
                           </div>
                           <Divider/> 
                           <div className="readmore">
                                 <Typography variant="body2" gutterBottom>
                                    Brand authorised ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur
                                    unde suscipit, quam beatae rerum <Link>...Read More</Link>
                                </Typography>
                           </div>
                           </div>
                           </div>
                           <div className="inner-wrp">
                           <div className="pro-wrp">
                           <Checkbox className="checkwrp"/>   
                           <div className="protection-content">
                            <div className="prot-first">
                               
                                <img src={images1.src} alt={images1.alt}></img>
                            </div>
                           <div className="prot-second">
                                <Typography variant="h6" gutterBottom> 1 Year Accidental, Liquid and Screen Production Plan</Typography>
                                <h5>3,847 <span><strike>6,800</strike></span> </h5>
                           </div>
                           </div>
                           <Divider/> 
                           <div className="readmore">
                                 <Typography variant="body2" gutterBottom>
                                    Brand authorised ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur
                                    unde suscipit, quam beatae rerum... <Link>Read More</Link>
                                </Typography>
                           </div>
                           </div>
                           </div>
                           <div className="inner-wrp">
                           <div className="pro-wrp">
                           <Checkbox className="checkwrp"/>
                           <div className="protection-content">
                            <div className="prot-first">
                                 <img src={images1.src} alt={images1.alt}></img>
                            </div>
                           <div className="prot-second">
                                <Typography variant="h6" gutterBottom> 1 Year Accidental, Liquid and Screen Production Plan</Typography>
                                <h5>3,847 <span><strike>6,800</strike></span> </h5>
                           </div>
                           </div>
                           <Divider/> 
                           <div className="readmore">
                                 <Typography variant="body2" gutterBottom>
                                    Brand authorised ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur
                                    unde suscipit, quam beatae rerum... <Link>Read More</Link>
                               
                           
                                </Typography>
                           </div>
                           </div>
                           </div>
                           </div>
                       
                           </div>
        
    );
}