import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import AddIcon from "@material-ui/icons/Add";
import TextField from "@material-ui/core/TextField";
import Box from "@material-ui/core/Box";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import './exchange.css';
import img1 from './images/exchange1.png';
import img2 from './images/exchange2.png';
import img3 from './images/exchange3.png';
const images1 = {
src:img1,
alt:'exchange1',
};
const images2 = {
src:img2,
alt:'exchange2',
};
const images3 = {
src:img3,
alt:'exchange3',
};
const brands = [
    {
        value: "0",
        label: "Choose brand",
    },
    {
        value: "1",
        label: "Apple",
    },
    {
        value: "2",
        label: "Realme",
    },
    {
        value: "3",
        label: "Nokia",
    },
    {
        value: "4",
        label: "Vivo",
    },
];
const modals = [
    {
        value: "0",
        label: "Choose modal",
    },
    {
        value: "1",
        label: "Apple 11 pro",
    },
    {
        value: "2",
        label: "Apple 12 mini ",
    },
    {
        value: "3",
        label: "Apple 12 pro",
    },
    {
        value: "4",
        label: "Apple iphone x",
    },
];
const styles = (theme) => ({
    
    closeButton: {
        position: "absolute",
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});

const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

export default 
{
	title:'Example/Exchangeoffer',
	component:Exchange,
};

export const Exchange = () =>
{
	const [open, setOpen] = React.useState(false);
    const [fullWidth, setFullWidth] = React.useState(true);
    const [maxWidth, setMaxWidth] = React.useState("md");
    const [currency, setCurrency] = React.useState("0");
    const [products, setProducts] = React.useState("0");
    const [state, setState] = React.useState({});
    const handleChange1 = (event) => {
        setCurrency(event.target.value);
    };
    const handleChange2 = (event) => {
        setProducts(event.target.value);
    };
    const handleChange = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
    };
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <Button variant="outlined" className="exchange_offer" onClick={handleClickOpen}>
                exchange offer
            </Button>
            <Dialog onClose={handleClose} fullWidth={fullWidth} maxWidth={maxWidth} aria-labelledby="customized-dialog-title" open={open}>
                <DialogTitle id="customized-dialog-title" onClose={handleClose}></DialogTitle>
                <DialogContent dividers>
                    <Grid container spacing={3}>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <Typography variant="h6" gutterBottom>
                                <b>Exchange offer</b>
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container spacing={3}>
                        <Grid item xs={12} sm={4} md={4} lg={4} className="exchange_work">
                            <div className="compare_adds">
                                <TextField
                                    id="outlined-select-currency-native"
                                    select
                                    label="Brands"
                                    value={currency}
                                    onChange={handleChange1}
                                    className="compare_choose"
                                    SelectProps={{
                                        native: true,
                                    }}
                                    variant="outlined"
                                >
                                    {brands.map((option) => (
                                        <option key={option.value} value={option.value}>
                                            {option.label}
                                        </option>
                                    ))}
                                </TextField>
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={4} md={4} lg={4} className="exchange_work">
                            <div className="compare_adds">
                                <TextField
                                    id="outlined-select-currency-native"
                                    select
                                    label="Modal"
                                    value={products}
                                    onChange={handleChange2}
                                    className="compare_choose"
                                    SelectProps={{
                                        native: true,
                                    }}
                                    variant="outlined"
                                >
                                    {modals.map((option) => (
                                        <option key={option.value} value={option.value}>
                                            {option.label}
                                        </option>
                                    ))}
                                </TextField>
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={4} md={4} lg={4} className="exchange_work">
                            <div>
                                <TextField id="outlined-error" label="IMEI Number" defaultValue="" variant="outlined" />
                            </div>
                        </Grid>
                    </Grid>
                    <Grid container spacing={3} className="exchange_divided">
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <Box p={1} className="exchange_head">
                                <Box p={1} className="flexing">
                                    <FormGroup row>
                                        <FormControlLabel
                                            control={<Checkbox icon={<CheckBoxOutlineBlankIcon fontSize="small" />} checkedIcon={<CheckBoxIcon fontSize="small" />} name="checkedI" />}
                                            label="I agree to the terms and conditions"
                                        />
                                    </FormGroup>
                                </Box>
                                <Box p={1}>
                                    <Button variant="contained" className="exchange_button">
                                        Exchange and Buy
                                    </Button>
                                </Box>
                            </Box>
                        </Grid>
                    </Grid>
                    <Grid container spacing={3} className="exchange_data">
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <Typography variant="h6" gutterBottom>
                                how exchange work?
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container spacing={3} className="exchange_data">
                        <Grid item xs={12} sm={4} md={4} lg={4} className="exchange_work">
                            <Typography variant="h5" gutterBottom>
                                Pick modal
                            </Typography>

                            <img src={images1.src} alt={images1.alt} />

                            <Typography variant="body1" gutterBottom>
                                Exchange phones all conditions.It will be verified
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={4} md={4} lg={4} className="exchange_work">
                            <Typography variant="h5" gutterBottom>
                                get value
                            </Typography>
                            <img src={images2.src} alt={images2.alt} />

                            <Typography variant="body1" gutterBottom>
                                Based on the checks,the exchange value can change
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={4} md={4} lg={4} className="exchange_work">
                            <Typography variant="h5" gutterBottom>
                                get order
                            </Typography>
                            <img src={images3.src} alt={images3.alt} />

                            <Typography variant="body1" gutterBottom>
                                If there's a change in value , you can pay the difference amount
                            </Typography>
                        </Grid>
                    </Grid>
                </DialogContent>
            </Dialog>
        </div>
    );
}