import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import HomeIcon from "@material-ui/icons/Home";
import Box from "@material-ui/core/Box";
import LocalOfferIcon from "@material-ui/icons/LocalOffer";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import { withStyles } from "@material-ui/core/styles";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Container from "@material-ui/core/Container";
import MyLocationIcon from "@material-ui/icons/MyLocation";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputAdornment from "@material-ui/core/InputAdornment";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import './address.css';

const State = [
    {
        value: "0",
        label: "TamilNadu",
    },
    {
        value: "1",
        label: "Karnataka",
    },
    {
        value: "2",
        label: "Kerala",
    },
    {
        value: "3",
        label: "Andra Pradesh",
    },
    {
        value: "4",
        label: "Hyderabed",
    },
];
function getSteps() {
    return ["Cart", "Address", "Payment"];
}

export default {
  title: 'Example/Address',
  component: AddAddress,
  
};
export const AddAddress = () =>
{



    const handleChange = (event) => {
        setValue(event.target.value);
    };
    const [currency, setCurrency] = React.useState("0");
    const [activeStep, setActiveStep] = React.useState(1);
    const steps = getSteps();
    const [values, setValues] = React.useState("");

    const handleChanges = (event) => {
        setValue(event.target.value);
    };
    const handleChanged = (event) => {
        setCurrency(event.target.value);
    };

    return (
        <div className="add_header">
            <Grid container>
                <Grid item xs={12} sm={12}>
                    <Stepper activeStep={activeStep} alternativeLabel>
                        {steps.map((label) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                </Grid>
            </Grid>

            <Grid container spacing={3} className="add_heads">
                <Grid item xs={12} sm={7} md={7} lg={7} className="add_wishlist">
                    <Box p={1} className="add_banner">
                        <Box p={1} className="add_flexing">
                            <HomeIcon />
                            My Addresses
                        </Box>
                    </Box>

                    <Grid container spacing={1} className="add_details">
                        <form className="add_forms" noValidate autoComplete="off">
                            <div>
                                <Button variant="contained" className="add_location">
                                    {" "}
                                    <MyLocationIcon />
                                    Use my current location
                                </Button>
                            </div>
                            <div>
                                <TextField id="outlined-textarea" label="Locality" className="add_labels" variant="outlined" />
                            </div>
                            <div className="add_text">
                                <FormControl fullWidth variant="outlined">
                                    <InputLabel htmlFor="outlined-adornment-amount">
                                        <b>Address</b>
                                    </InputLabel>
                                    <OutlinedInput id="outlined-adornment-amount" className="add_address" value={values} onChange={handleChange} labelWidth={70} rowsMax={4} />
                                </FormControl>
                            </div>
                            <div>
                                <TextField id="outlined-multiline-flexible" className="add_labels" label="City" rowsMax={1} value={values} onChange={handleChanges} variant="outlined" />
                                <TextField
                                    id="outlined-textarea"
                                    select
                                    label="State"
                                    className="add_labels"
                                    variant="outlined"
                                    value={currency}
                                    onChange={handleChanged}
                                    SelectProps={{
                                        native: true,
                                    }}
                                >
                                    {State.map((option) => (
                                        <option key={option.value} value={option.value}>
                                            {option.label}
                                        </option>
                                    ))}
                                </TextField>
                            </div>
                           
                            <div>
                                <RadioGroup row className="add_radio">
                                    <FormLabel component="legend">Address Type:</FormLabel>
                                    <FormControlLabel value="Home" control={<Radio color="primary" />} label="Home" />
                                    <FormControlLabel value="Office" control={<Radio color="primary" />} label="Office" />
                                    <FormControlLabel value="Other" control={<Radio color="primary" />} label="Other" />
                                </RadioGroup>
                            </div>
                            <div>
                                <Button variant="contained" className="add_buttons">
                                    Save and Deliver here{" "}
                                </Button>
                                <Button variant="contained" className="add_cancels">
                                    cancel
                                </Button>
                            </div>
                        </form>
                    </Grid>
                </Grid>

                <Grid item xs={12} sm={5} md={5} lg={5} className="add_sum">
                    <div className="add_payments">
                        <Typography variant="h6" gutterBottom>
                            Payment Summary
                        </Typography>

                        <div className="add_price">
                            <Box className="add_summary" p={1}>
                                <Box p={1} className="add_flexing">
                                    Price - 2 Items
                                </Box>
                                <Box p={1}>1,06,600</Box>
                            </Box>
                            <Box className="add_summary" p={1}>
                                <Box p={1} className="add_flexing">
                                    Delivery Fees
                                </Box>
                                <Box p={1} className="delivery">
                                    Free
                                </Box>
                            </Box>
                            <Box className="add_summary" p={1}>
                                <Box p={1} className="add_flexing">
                                    Coupon Discount
                                </Box>
                                <Box p={1} className="add_coupon">
                                    Apply Coupon
                                </Box>
                            </Box>

                            <div className="add_total">
                                <Box className="add_summary" p={1}>
                                    <Box p={1} className="add_flexing">
                                        Total Amount
                                    </Box>
                                    <Box p={1}>1,06,600</Box>
                                </Box>
                                <Typography variant="subtitle2" gutterBottom>
                                    <LocalOfferIcon fontSize="small" />
                                    You will be save 5,000 in this order
                                </Typography>
                            </div>
                        </div>
                    </div>

                    <Button variant="contained" className="add_payment">
                        Proceed To Checkout
                    </Button>
                </Grid>
            </Grid>
        </div>
    );
}