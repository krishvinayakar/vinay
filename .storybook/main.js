module.exports = {
  "stories": [
    "../stories/**/*.stories.mdx",
    "../stories/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-events",
    "@storybook/addon-a11y",
    "@storybook/addon-cssresources",
    
  ],
   addons: [
    {
      name: '@storybook/addon-storysource',
      options: {
        rule: {
           test: [/\.stories\.jsx?$/], This is default
          //include: [path.resolve(__dirname, '../src')], // You can specify directories
        },
        loaderOptions: {
          prettierConfig: { printWidth: 80, singleQuote: false },
        },
      },
    },
  ],

}