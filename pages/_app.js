import '../styles/globals.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faAngleDoubleDown, faAngleDoubleUp, faEquals,faRupeeSign,faGift } from '@fortawesome/free-solid-svg-icons';
// This default export is required in a new `pages/_app.js` file.
function MyApp({ Component, pageProps }) {
  library.add(faAngleDoubleDown, faAngleDoubleUp, faEquals,faRupeeSign,faGift)
  return <Component {...pageProps} />
}

export default MyApp




